# Visual Assist X VS2019 资源文件

本仓库提供了一个适用于 Visual Studio 2019 社区版的 Visual Assist X 资源文件。该资源文件在 Windows 7 环境下亲测可用，主要用于替换 `VA_X.dll` 文件，以确保 Visual Assist X 在 Visual Studio 2019 中正常运行。

## 使用说明

1. **下载资源文件**：
   - 从本仓库中下载 `VA_X.dll` 文件。

2. **替换文件**：
   - 关闭 Visual Studio 2019。
   - 找到 Visual Assist X 的安装目录，通常位于 `C:\Program Files (x86)\Visual Assist\` 或 `C:\Program Files\Visual Assist\`。
   - 将下载的 `VA_X.dll` 文件替换到该目录中。

3. **关闭自动升级**：
   - 打开 Visual Studio 2019。
   - 进入 `工具` -> `扩展和更新`。
   - 找到 Visual Assist X，并确保关闭其自动升级功能，以防止许可证到期。

## 注意事项

- 请确保在替换 `VA_X.dll` 文件之前关闭 Visual Studio 2019，以避免文件被占用。
- 关闭 Visual Assist X 的自动升级功能非常重要，否则可能会导致许可证到期，影响使用。

## 支持环境

- 操作系统：Windows 7
- Visual Studio 版本：2019 社区版

## 反馈与问题

如果在使用过程中遇到任何问题或有任何建议，欢迎在仓库中提交 Issue 或 Pull Request。

感谢您的使用！